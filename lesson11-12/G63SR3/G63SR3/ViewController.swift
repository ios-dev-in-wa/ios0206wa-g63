//
//  ViewController.swift
//  G63SR3
//
//  Created by Ivan Vasilevich on 6/30/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var textField: UITextField!
	@IBOutlet weak var label: UILabel!
	@IBOutlet weak var textView: UITextView!
	
	var result = 0.0
	
	let kResult = "kResult"
	let kHistory = "kHistory"
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		label.text = UserDefaults.standard.double(forKey: kResult).description
		textView.text = UserDefaults.standard.string(forKey: kHistory)
	}

	@IBAction func buttonPressed() {
		print(textField.text!)
		guard let numberFromTextField = Double(textField.text!) else {
			print("enter correct number to textField")
			return
		}
		guard let numberFromTextView = Double(textView.text!) else {
			print("enter correct number to textView")
			return
		}
		result = numberFromTextField + numberFromTextView
		textView.font = UIFont.systemFont(ofSize: CGFloat(result))
		label.text = result.description
		UserDefaults.standard.set(result, forKey: kResult)
		
		let historyString = "\(numberFromTextField) + \(numberFromTextView) = \(result)\n"
		var oldResults = UserDefaults.standard.string(forKey: kHistory) ?? ""
		oldResults += historyString
		UserDefaults.standard.set(oldResults, forKey: kHistory)
	}
	

}

