//
//  ViewController.swift
//  G63L11
//
//  Created by Ivan Vasilevich on 7/7/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

func log(_ functionName: String = #function, line: Int = #line, file: String = #file, message: String = "") {
	let dateFormatter = DateFormatter()
	dateFormatter.dateFormat = "HH:mm:ss"
	print("\(dateFormatter.string(from: Date())) l#:\(line) \(functionName) in \((file as NSString).lastPathComponent) " + message)
}

import UIKit
import SDWebImage

class ViewController: UIViewController {
	

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		Downloader().sendWeatherRequest()
	}
	
	@IBAction func downloadImage() {
		log()
		let linkStr = "http://speedhunters-wp-production.s3.amazonaws.com/wp-content/uploads/2018/06/29051704/2018-Players-Classic-VW-Caddy-TFSI-for-Speedhunters-by-Paddy-McGrath-5.jpg"
		let url = URL(string: linkStr)!
		Downloader().downloadImage(url: url) { (image, error) in
			let imageView =  self.view.viewWithTag(5) as? UIImageView
			imageView?.image = image
		}
		log()
	}
	
//	@IBAction func downloadImage() {
//		log()
//		let linkStr = "http://speedhunters-wp-production.s3.amazonaws.com/wp-content/uploads/2018/06/29051704/2018-Players-Classic-VW-Caddy-TFSI-for-Speedhunters-by-Paddy-McGrath-5.jpg"
//		let url = URL(string: linkStr)!
//		let imageView = view.viewWithTag(5) as? UIImageView
//		imageView?.sd_setImage(with: url, completed: { (image, error, cacheType, url) in
//			print(image?.size ?? .zero)
//		})
//		TimeInterval
//		log()
//	}
	

	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		downloadImage()
	}


}

