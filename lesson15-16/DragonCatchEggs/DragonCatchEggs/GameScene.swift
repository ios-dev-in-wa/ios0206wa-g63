//
//  GameScene.swift
//  DragonCatchEggs
//
//  Created by Ivan Vasilevich on 8/30/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    private var label : SKLabelNode?
//    private var spinnyNode : SKShapeNode?
	private var hero: SKSpriteNode {
		return self.childNode(withName: "hero") as! SKSpriteNode
	}
	private var score = 0
	
	let eggsNames = ["egg", "dragonEgg", "pockeBall", "hart"]
    
    override func didMove(to view: SKView) {
        
        // Get label node from scene and store it for use later
        self.label = self.childNode(withName: "//helloLabel") as? SKLabelNode
        if let label = self.label {
            label.alpha = 0.0
            label.run(SKAction.fadeIn(withDuration: 2.0))
        }
		
		self.run(SKAction.playSoundFileNamed("bgMusic", waitForCompletion: false))
		
		Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(dropEgg), userInfo: nil, repeats: true)
		
    }

	@objc func dropEgg() {
		
		let randomIndex = Int(arc4random()%UInt32(eggsNames.count))
		let eggName = eggsNames[randomIndex]
		print(eggName)
		let egg = SKSpriteNode(imageNamed: eggName)
		egg.position.y = self.size.height
		egg.position.x = CGFloat(CGFloat(arc4random()%UInt32(self.size.width)) - self.size.width/2)
		egg.size = CGSize(width: 120, height: 120)
		egg.zPosition += 1
		self.addChild(egg)
		
		let soundAction = SKAction.playSoundFileNamed("eggDropped.wav", waitForCompletion: false)
		let actionGoToHero = SKAction.moveTo(y: hero.position.y, duration: 0.5)
		let decideAct = SKAction.customAction(withDuration: 0) { (node, someNumber) in
			if self.hero.frame.contains(node.frame) {
				self.score += 1
//				self.score = self.score + 1
				let soundAction = SKAction.playSoundFileNamed("catchedEgg.wav", waitForCompletion: false)
				node.run(SKAction.sequence([soundAction, SKAction.removeFromParent()]))
			}
			else {
				self.score -= 1
				let goDown = SKAction.moveTo(y: self.size.height/2 - 60, duration: 0.07)
				let changeSprite = SKAction.customAction(withDuration: 0, actionBlock: { (node, someNumber) in
					let omlet = SKSpriteNode(imageNamed: "brokenEgg")
					omlet.size = CGSize(width: 150, height: 150)
					omlet.position = node.position
					omlet.position.y = -self.size.height/2 + CGFloat(arc4random()%30)
					self.addChild(omlet)
					omlet.run(SKAction.playSoundFileNamed("brokenEgg", waitForCompletion: false))
					node.removeFromParent()
				})
				let seq = SKAction.sequence([goDown, changeSprite])
				egg.run(seq)
			}
			self.label?.text = self.score.description
		}
		let seq = SKAction.sequence([soundAction, actionGoToHero, decideAct])
		egg.run(seq)
		
	}
	
    func touchMoved(toPoint pos : CGPoint) {
		hero.position.x = pos.x
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
			self.touchMoved(toPoint: t.location(in: self))
			
		}
    }
	
}
