//
//  DetailViewController.swift
//  G63L13
//
//  Created by Ivan Vasilevich on 7/14/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

	@IBOutlet weak var detailDescriptionLabel: UILabel!


	func configureView() {
		// Update the user interface for the detail item.
		let date = Date()
		let b = Boot.shared
		
		b.name = "Sandal"
		print(b.labelLenght)
		print(date.timeIntervalSinceNow)
		if let detail = detailItem {
		    if let label = detailDescriptionLabel {
		        label.text = detail.description
		    }
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		NotificationCenter.default.addObserver(self,
											   selector: #selector(disPlayUsedItem(notification:)),
											   name: NSNotification.Name(rawValue: Notifications.overuseNotification.rawValue),
											   object: nil)
		
		configureView()
	}


	var detailItem: NSDate? {
		didSet {
		    // Update the view.
		    configureView()
		}
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		Boot.shared.use()
	}
	
	@objc func disPlayUsedItem(notification: Notification) {
		let message = notification.userInfo?[Keys.kOveruseMessage] as? String
		detailDescriptionLabel.text = message
		view.backgroundColor = UIColor.red.withAlphaComponent(0.7)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self)
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}


}

