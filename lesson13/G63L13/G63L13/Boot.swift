//
//  Boot.swift
//  G63L13
//
//  Created by Ivan Vasilevich on 7/14/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit



class Boot: NSObject {
	
	static let shared = Boot()
	
	var name: String?
	
	var useCount: Int = 0 {
//		get {
//		 	return useCount
//		UserDefaults.standard

//		}
		didSet {
			if useCount < 0 {
				useCount = oldValue
			}
			if useCount > 3 {
				sendOverUseNotification()
			}
		}
	}
	
	private func sendOverUseNotification() {
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.overuseNotification.rawValue), object: self, userInfo: [Keys.kOveruseMessage : "boot was overused"])
	}
	
	var labelLenght: Int {
		let result = (name?.count ?? 0) * 2
		return result
	}
	
	lazy var bootImage: UIImage? = {
		let path = Bundle.main.path(forResource: "img068", ofType: "jpg")!
		let data = NSData.init(contentsOfFile: path) as! Data
		let image = UIImage.init(data: data)
		return image
	}()
	
	func use()  {
		useCount += 1
		
	}
	
	
	
	

}
