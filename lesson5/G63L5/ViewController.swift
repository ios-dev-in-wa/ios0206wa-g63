//
//  ViewController.swift
//  G63L5
//
//  Created by Ivan Vasilevich on 6/16/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		playWithPhones()
		var result = ""
		print("123")
		result += "123".description
		print(123)
		result += 123.description
		print([1, 2, 3])
		result += [1, 2, 3].description
		
		fA()
		a = 10
		fB(a:10)
	}
	
	func printHello() {
		print("hello")
	}
	
	func playWithPhones() {
		let myPhone = Phone(screen: 5.5, memory: 32)
		print(myPhone.photosCount)
//		myPhone.photosCount = 8
		print(myPhone.description)
		
		let myPhone2 = Phone(screen: 3.5, memory: 16)
		myPhone2.makePhoto()
		myPhone2.makePhoto()
		myPhone2.makePhoto()
		print(myPhone2.description)
		
	}
	
	
	var a = 0
	
	func fA() {
		a = 5
	}
	
	func fB(a: Int) {
		print(self.a)
		print(a)
	}


}

