//
//  ViewController.swift
//  G63L9
//
//  Created by Ivan Vasilevich on 6/30/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	let text = "ViewController.swift G63L9 Created by Ivan Vasilevich on 6/30/18. Copyright © 2018 RockSoft. All rights reserved."
	var words: [String] {
		let result = text.components(separatedBy: " ")
		if query.isEmpty {
			return result
		}
		else {
//			return result.filter({ (element) -> Bool in
//				return element.contains(query)
//			})
			return result.filter({ $0.contains(query) })
			
			
		}
	}
	
	var query = ""
	
	@IBOutlet weak var searchTextField: UITextField!
	@IBOutlet weak var tableView: UITableView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		searchTextField.delegate = self
		tableView.dataSource = self
		tableView.delegate = self
	}

	@IBAction func loginPressed(_ sender: UIBarButtonItem) {
		performSegue(withIdentifier: "showLogin", sender: nil)
	}
	
	@IBAction func queryChanged(_ sender: UITextField) {
		query = sender.text!
		tableView.reloadData()
	}
	
}

extension ViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let wordToDisplay = words[indexPath.row]
		let wordDisplayVC = storyboard!.instantiateViewController(withIdentifier: "wordDisplayVC")
		wordDisplayVC.title = wordToDisplay
		navigationController?.pushViewController(wordDisplayVC, animated: true)
	}
}

extension ViewController: UITableViewDataSource {
	public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return words.count
	}
	
	
	public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "wordCell", for: indexPath)
		let wordToDisplay = words[indexPath.row]
		cell.textLabel?.text = wordToDisplay
		return cell
	}

}

extension ViewController: UITextFieldDelegate {
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//		view.endEditing(true)
//		textField.becomeFirstResponder()
		textField.resignFirstResponder()
		return false
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		query = textField.text! + string
		tableView.reloadData()
		print("searchQuery",  query)
		return true
	}
	
}

