//
//  ViewController.swift
//  G63L2Functions
//
//  Created by Ivan Vasilevich on 6/6/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let g63PersonCount = 10
		let g64PersonCount = 3
		sayGoodMorning()
		print("=============================")
		sayGoodMorning(participantsCount: g63PersonCount)
		print("=============================")
		let g63PencilsCount = countStudentsPencil(studentsCount: g63PersonCount, maxPencilsCount: 2)
		print("G63 countStudentsPencilCount = \(g63PencilsCount)")
		
		let g64PencilsCount = countStudentsPencil(studentsCount: g64PersonCount, maxPencilsCount: 5)
		print("G64 countStudentsPencilCount = \(g64PencilsCount)")
	}
	
	func countStudentsPencil(studentsCount: Int, maxPencilsCount: UInt32) -> Int {
		
		var result = 0
		for _ in 0..<studentsCount {
			result = result + Int(arc4random()%maxPencilsCount) + 1
		}
		return result
		
	}
	
	func countStudentsPencil(studentsCount: Int) -> Int {

		var result = 0
		for _ in 0..<studentsCount {
			result = result + Int(arc4random()%2) + 1
		}
		return result
		
	}
	
	func countStudentsPencil() -> Int {
		
		var result = 0
		let studentsCount = 10
		for _ in 0..<studentsCount {
			result = result + Int(arc4random()%2) + 1
		}
		return result
		
	}
	
	func sayGoodMorning(participantsCount: Int) {
		
		for i in 0..<participantsCount {
			goodMorning(number: i + 1)
		}
		
	}
	
	func sayGoodMorning() {
		
		let participantsCount = 3
		for i in 0..<participantsCount {
			goodMorning(number: i + 1)
		}
		
	}
	
	func goodMorning(number: Int) {
		print("good french morning \(number)")
	}

}

