//
//  IntoVC.swift
//  G63L14_2
//
//  Created by Ivan Vasilevich on 7/15/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class IntoVC: UIViewController {
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var pageControll: UIPageControl!
	
	let imagesNames = ["0", "1", "2"]
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		setupIntro()
	}
	
	private func setupIntro() {
		
		scrollView.contentInset.left = 20
		
		scrollView.contentSize.width = CGFloat(imagesNames.count) * view.frame.width
		for i in 0..<imagesNames.count {
			let imageView = UIImageView.init(frame: view.frame)
			imageView.frame.origin.x = view.frame.width * CGFloat(i)
			imageView.contentMode = .scaleAspectFit
			imageView.image = UIImage.init(named: i.description)
			scrollView.addSubview(imageView)
		}
	}
}

extension IntoVC: UIScrollViewDelegate {
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		pageControll.currentPage = Int(scrollView.contentOffset.x / scrollView.frame.width)
	}
	
}
