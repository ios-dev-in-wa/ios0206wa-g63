//
//  Downloader.swift
//  G63L11
//
//  Created by Ivan Vasilevich on 7/7/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class Downloader: NSObject {
	
	static let shared = Downloader()
	
	typealias CompletionImageBlock = (_ result: UIImage?, _ error: Error?) -> Void
	
	func downloadImage(url: URL, completion: @escaping CompletionImageBlock) {
			let linkStr = "http://speedhunters-wp-production.s3.amazonaws.com/wp-content/uploads/2018/06/29051704/2018-Players-Classic-VW-Caddy-TFSI-for-Speedhunters-by-Paddy-McGrath-5.jpg"
			let url = URL(string: linkStr)!
		kBgQ.async {
			var img: UIImage?
			var err: Error?
			do {
				let data = try Data.init(contentsOf: url)
				let image = UIImage.init(data: data)
				img = image
			}
			catch {
				print(error)
				err = error
			}
			kMainQueue.async {
				completion(img, err)
			}
			
		}
		
		}



}
