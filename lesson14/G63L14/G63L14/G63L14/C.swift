//
//  C.swift
//  G63L13
//
//  Created by Ivan Vasilevich on 7/14/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import Foundation

enum Notifications: String {
	case overuseNotification = "OveruseNotification"
}

struct Keys {
	static let kOveruseMessage = "message"
}

