//
//  ViewController.swift
//  G63L14
//
//  Created by Ivan Vasilevich on 7/15/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var imageBox: UIImageView!
	
	@objc var boot = Boot()

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		addObserver(self, forKeyPath: #keyPath(boot.name), options: [.old, .new], context: nil)
//		let dict = ["1" : 11] as NSDictionary
		boot.setValue("Roach", forKey: "name")
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let number = Date().timeIntervalSince1970//1531642040.0
		let date = Date.init(timeIntervalSince1970: number)
		print(date)
		let formatter = DateFormatter()
		formatter.dateFormat = "EEE MMM d, H:mm"
		print(formatter.string(from: date))
		boot.name = number.description
		
		Downloader.shared.downloadImage(url: URL(string: "https://www.youtube.com/watch?v=0LoKDDRlfZc&index=2&list=PLg8KZgTw-gyGqG2AQb66QWsTevTZ92I7p")!) { (image: UIImage?, error: Error?) in
				self.imageBox.image = image
		}
	}
	
	// MARK: - Key-Value Observing
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		if keyPath == #keyPath(boot.name) {
			// Update Time Label
			print("boot name changed to\"\(boot.name!)\"")
		}
	}

}

