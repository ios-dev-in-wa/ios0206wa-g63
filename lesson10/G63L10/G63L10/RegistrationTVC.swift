//
//  RegistrationTVC.swift
//  G63L10
//
//  Created by Ivan Vasilevich on 7/1/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class RegistrationTVC: UITableViewController {
	
	@IBOutlet weak var topCell: UITableViewCell!
	@IBOutlet var sec0Cells: [UITableViewCell]!
	@IBOutlet var sec1Cells: [UITableViewCell]!
	@IBOutlet var sec2Cells: [UITableViewCell]!
	
	var allStatic: [[UITableViewCell]] {
		return [sec0Cells, sec1Cells, sec2Cells, sec2Cells]
	}
    override func viewDidLoad() {
        super.viewDidLoad()

        let box = FaceView(frame: CGRect(x: 10, y: 25, width: 310, height: 310))
		box.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
									  green: CGFloat(arc4random_uniform(256))/255,
									  blue: CGFloat(arc4random_uniform(256))/255,
									  alpha: 1)
		box.faceColor = .orange
		topCell.contentView.addSubview(box)
		
    }

	@IBAction func longPressed(_ sender: UILongPressGestureRecognizer) {
		navigationController?.popViewController(animated: true)
	}
	
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allStatic[section].count
    }

	

}
