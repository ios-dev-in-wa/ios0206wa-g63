//
//  SelectAnimalVC.swift
//  G63L8
//
//  Created by Ivan Vasilevich on 6/24/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class SelectAnimalVC: UIViewController {

	@IBOutlet weak var nameTextField: UITextField!
	@IBOutlet weak var missedCallsSegmentControll: UISegmentedControl!
	
	//	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
	//		<#code#>
	//	}
	
	override func awakeFromNib() { //xib // nib
		super.awakeFromNib()
		log()
		print(missedCallsSegmentControll)
		
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		log()
		print(missedCallsSegmentControll)
		nameTextField.text = "Vanya"
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		log()
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		log()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		log()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		log()
		//		tabBarItem.badgeValue = ""
		navigationController?.tabBarItem.badgeValue = nil
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		log()
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		log()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
		log()
		//		model.clearCache
	}
	
	let showDetailAnimalSegueId = "showDetailAnimal"
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let selectedAnimalIndex = missedCallsSegmentControll.selectedSegmentIndex
		let selectedAnimal = missedCallsSegmentControll.titleForSegment(at: selectedAnimalIndex)
		performSegue(withIdentifier: showDetailAnimalSegueId, sender: selectedAnimal)
		log()
	}
	
	@IBAction func showResult(_ sender: UISegmentedControl) {
		let congratzString = "Welcome: \(nameTextField.text!) is  \(sender.selectedSegmentIndex == 0 ? "krosafcheg" : "idiot")"
		performSegue(withIdentifier: "go to green VC", sender: congratzString)
	}
	
	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		log()
//		print(sender as! NSObject)
		if segue.identifier == showDetailAnimalSegueId {
			if let animalName = sender as? String {
				let animalDetailVC = segue.destination as! AnimalDetailVC
				animalDetailVC.animalName = animalName
			}
		}
		
		//			if let senderAsString = sender as? String {
		//				print(senderAsString)
		//				if let greenVC = segue.destination as? GreenVC {
		//					greenVC.stringFromPrevVC = senderAsString
		//				}
		//			}
		//
		//			print(segue.identifier ?? "no segue id")
		//		}
		
	}

}
