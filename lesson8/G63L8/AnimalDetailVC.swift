//
//  AnimalDetailVC.swift
//  G63L8
//
//  Created by Ivan Vasilevich on 6/24/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class AnimalDetailVC: UIViewController {
	@IBOutlet weak var animalPhotoImageView: UIImageView!
	
	var animalName = ""
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
		animalPhotoImageView.image = UIImage(named: animalName.lowercased())
		animalPhotoImageView.contentMode = .scaleAspectFit
		
		
		setupNavigationItem()
    }
	
	func setupNavigationItem() {
		navigationItem.title = animalName
		let button = UIBarButtonItem(image: #imageLiteral(resourceName: "alligator"), style: .plain, target: self, action: #selector(foo))
		navigationItem.rightBarButtonItem = button
	}
	
	@objc func foo() {
		print("crocodila")
	}

	

}
