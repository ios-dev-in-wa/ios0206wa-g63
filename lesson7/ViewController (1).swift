//
//  ViewController.swift
//  G63L7
//
//  Created by Ivan Vasilevich on 6/23/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var topImageView: UIImageView!
	
	let filePath = "/Users/ivanvasilevich/Desktop/array.plist"

	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		propertyListExample()
		readFromPropertyList()
		userDefaultsWork()
		view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
									   green: CGFloat(arc4random_uniform(256))/255,
									   blue: CGFloat(arc4random_uniform(256))/255,
									   alpha: 1)
		view.backgroundColor = UIColor.init(rgb: 0xff5400)
		topImageView.image = UIImage(named: "cloud")
//		topImageView.image = #imageLiteral(resourceName: "cloud")
	}
	
	func propertyListExample() {
		let numbers: NSArray = [1, 2, 4, 8, 11]
		numbers.write(toFile: filePath, atomically: false)
	}
	
	func readFromPropertyList() {
		let numbersFromFile = NSArray(contentsOfFile: filePath)
		print(numbersFromFile!)
	}
	
	func userDefaultsWork() {
		let kRunCount = "runCount"
		let number = UserDefaults.standard.integer(forKey: kRunCount)
		print("runCount = \(number)")
		UserDefaults.standard.set(number + 1, forKey: kRunCount)
	}

}

extension UIColor {
	convenience init(red: Int, green: Int, blue: Int) {
		assert(red >= 0 && red <= 255, "Invalid red component")
		assert(green >= 0 && green <= 255, "Invalid green component")
		assert(blue >= 0 && blue <= 255, "Invalid blue component")
		
		self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
	}
	
	convenience init(rgb: Int) {
		self.init(
			red: (rgb >> 16) & 0xFF,
			green: (rgb >> 8) & 0xFF,
			blue: rgb & 0xFF
		)
	}
}
